import glob
import os
import platform
import re

import cv2
import numpy as np
import array as arr
import ImageManipulation as IMan

FILE_LIMIT = 2


def getfiles(keyword):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print("Current directory: ", dir_path)
    os.chdir(dir_path)
    files = []
    paths = []
    # for indx, file in enumerate(glob.glob(keyword)):
    #     files.append(file)
    #     print(indx, file.title())

    for (dirpath, dirnames, filenames) in os.walk(dir_path):
        os.chdir(dirpath)

        for file in glob.glob(keyword):
            if file.endswith('.jpg') or file.endswith('.png'):
                if len(paths) == 0 or dirpath != paths[-1]:
                    paths.append(dirpath)
                if platform.system() == 'Windows':
                    files.append(dirpath + "\\" + file)
                else:
                    files.append(dirpath + "/" + file)

                # list_of_files[filename] = os.sep.join([dirpath, filename])

    return files, paths


def imageSelect(files, display=False):
    while True:
        for indx, file in enumerate(files):
            print(indx, file)
        try:
            infile = int(input('Select an input file: '))
            file = files[infile]
            break
        except ValueError:
            print("not a number")
        except IndexError:
            print("number out of range")

    print(file)

    image = cv2.imread(file, 1)
    if display:
        cv2.namedWindow('image', cv2.WINDOW_GUI_NORMAL)
        cv2.imshow('image', image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        return image
    else:
        return image


def imageSetSelection(paths):
    while True:
        for indx, path in enumerate(paths):
            print(indx, path)
        try:
            infile = int(input('Select an input path: '))
            setpath = paths[infile]
            os.chdir(setpath)
            break
        except ValueError:
            print("not a number")
        except IndexError:
            print("number out of range")

    images = []
    image_files = []
    for file in glob.glob("*.*"):
        if file.endswith('.jpg') or file.endswith('.png'):

            image_files.append(file)
            # images.append(cv2.imread(file, 1))

    for file in sorted(image_files):
        print(file)
        images.append(cv2.imread(file, 1))
    # images.sort(key=lambda f: int(re.sub('\D', '', f)))
    return images


def loopagain():
    while True:
        try:
            y = input('continue? (-1 to exit)  ')
            if y == '':
                return True
            return not (int(y) == -1)
        except ValueError:
            print("not a number")


def main():
    sift = cv2.xfeatures2d.SIFT_create()
    threshold = 0.05
    while True:
        files, paths = getfiles("*.*")
        # image = imageSelect(files, False)
        image_set = imageSetSelection(paths)
        threshold = 0.05
        while True:
            try:
                print("Threshold (current = % .3f): " % threshold)
                y = input()
                if y == '':
                    break
                threshold = float(y)
                break
            except ValueError:
                print("not a number")
        sift = cv2.xfeatures2d.SIFT_create()
        bf = cv2.BFMatcher()
        images_info = []
        print("Processing Images ....")
        indx = 0
        for indx, image in enumerate(image_set):

            img = image

            kps, desc = sift.detectAndCompute(image, None)
            goodkps = []
            descarr = desc[0]
            first = True
            for i, keypoint in enumerate(kps):
                if keypoint.response > threshold:
                    goodkps.append(keypoint)
                    # image_info[0].remove(tmpinfo[0][i])
                    if first:
                        descarr = desc[i]
                        first = False
                    else:
                        descarr = np.vstack([descarr, desc[i]])

            images_info.append((goodkps, descarr))
            img = cv2.drawKeypoints(image, images_info[indx][0], img, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
            cv2.namedWindow("Image number {}".format(indx))
            cv2.imshow("Image number {}".format(indx), img)

            if indx == FILE_LIMIT - 1:
                break

        matches = bf.knnMatch(images_info[0][1], images_info[1][1], k=2)
        stitcher = IMan.Stitcher()

        # Apply ratio test
        good = []

        for m, n in matches:
            if m.distance < 0.60 * n.distance:
                good.append([m])

        H, invH, inlier_matches = stitcher.RANSAC(good, images_info, 10, 1e-1)

        if len(inlier_matches) < 1:
            print("\n\ninlier matches: NO MATCHES\n\n")
            continue

        print("inlier matches: ", len(inlier_matches))

        img3 = cv2.drawMatchesKnn(image_set[0], images_info[0][0], image_set[1], images_info[1][0], inlier_matches, None, flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
        indx += 1
        cv2.namedWindow("Image number {}".format(indx))
        cv2.imshow("Image number {}".format(indx), img3)

        stitched_img = stitcher.stitch(image_set, H, invH)

        indx += 1
        cv2.imwrite('../Results/stitchedimage.png', stitched_img)
        cv2.namedWindow("Image number {}".format(indx))
        cv2.imshow("Image number {}".format(indx), stitched_img)



        cv2.waitKey(0)
        cv2.destroyAllWindows()
        if not loopagain():
            break


if __name__ == "__main__":
    main()
