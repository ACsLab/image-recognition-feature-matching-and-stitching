import numpy as np
import cv2
from numpy.random import seed
from numpy.random import randint

class Stitcher:
    # create the sift feature detector
    sift = cv2.xfeatures2d.SIFT_create()
    bfm = cv2.BFMatcher()
    # the object that will store the keypoints that are returned from sift
    keyPoints = None

    def __init__(self):
        print("image Stitcher created")

    #
    #   projects the point x1,y1 using the homography
    #   and returns the projected points x2 y2
    #
    def project(self, x1, y1, homography):
        w = homography[2][0] * x1 + homography[2][1] * y1 + homography[2][2]
        x2 = (homography[0][0] * x1 + homography[0][1] * y1 + homography[0][2]) / w
        y2 = (homography[1][0] * x1 + homography[1][1] * y1 + homography[1][2]) / w

        return x2, y2

    #
    #   helper function for ransac that computes the number of inlying points
    #   given a homography. returns the total number of inliers.
    #   project the first point in each match using the function project
    #   if the projected point is less then the inlier threshold from the second point
    #   it is considered an inlier
    #
    def computeInlierCount(self, homography, src_pts, dst_pts, inlierThreshold):

        numinliers = 0

        for pt, dst_pt in zip(src_pts, dst_pts):
            x_p, y_p = self.project(pt[0][0], pt[0][1], homography)

            dist = np.absolute(np.sqrt((dst_pt[0][0] - x_p) ** 2 + (dst_pt[0][1] - y_p) ** 2))

            if dist < inlierThreshold:
                numinliers += 1

        return numinliers

    #
    #
    #   returns the inlier pts from the homography and pts given
    #
    def computeAllInliers(self, homography, src_pts, dst_pts, inlierThreshold):

        inlier_src_pts, inlier_dst_pts, inlier_indeces = np.zeros([]), np.zeros([]), []
        first = True
        for pt, dst_pt, qindex in zip(src_pts, dst_pts, range(len(src_pts))):
            x_p, y_p = self.project(pt[0][0], pt[0][1], homography)

            dist = np.absolute(np.sqrt((dst_pt[0][0] - x_p) ** 2 + (dst_pt[0][1] - y_p) ** 2))

            if dist < inlierThreshold:
                if first:
                    inlier_src_pts = np.vstack(pt)
                    inlier_dst_pts = np.vstack(dst_pt)
                    inlier_indeces.append(qindex)
                    first = False
                else:
                    inlier_src_pts = np.vstack([inlier_src_pts, pt])
                    inlier_dst_pts = np.vstack([inlier_dst_pts, dst_pt])
                    inlier_indeces.append(qindex)

        return inlier_src_pts, inlier_dst_pts, inlier_indeces

    #
    #   this function takes a list of potentially matching
    #   points between two images and returns the homography
    #   transformation that relates them
    #
    def RANSAC(self, matches, image_info, numIterations, inlierThreshold):

        if len(matches) < 1:
            return 0, 0, []

        seed(1)
        inliers_biggest = 0
        bestH = np.zeros([])

        queryindxs = np.float32([m[0].queryIdx for m in matches])
        srcpts = np.float32([image_info[0][0][m[0].queryIdx].pt for m in matches]).reshape(-1, 1, 2)
        dstpts = np.float32([image_info[1][0][m[0].trainIdx].pt for m in matches]).reshape(-1, 1, 2)
        for x in range(numIterations):

            # randomly select 4 pairs of potentially matching points from matches
            values = randint(0, len(matches), 4)
            # print(values)

            # position of keypoint in image_info
            srcpts_selection = srcpts[values]

            dstpts_selection = dstpts[values]

            # print(srcpts_selection, ",  ")
            # print(dstpts_selection, ", ")

            # compute the homoography relating the four selected matches with cv2.findHomography
            H, mask = cv2.findHomography(srcpts_selection, dstpts_selection, 0)

            inliers = self.computeInlierCount(H, srcpts_selection, dstpts_selection, inlierThreshold)

            #print("numInliers: ", inliers)
            # store the homography with the highest number of inliers
            if inliers_biggest < inliers:
                bestH = H

        # get all points ins srcpts and dstpts that are inliers using the best homography
        inlier_src_pts, inlier_dst_pts, inlier_indeces = self.computeAllInliers(bestH, srcpts, dstpts, inlierThreshold)

        # compute a new homography using all the inleirs found in the last step
        homography, mask = cv2.findHomography(inlier_src_pts, inlier_dst_pts, 0)

        # compute an inverse homography
        invhomography = np.linalg.inv(homography)

        # inlier_indeces = np.array(inlier_indeces, np.int)
        inlier_matches = [matches[i_idx] for i_idx in inlier_indeces]

        return homography, invhomography, inlier_matches

    def stitch(self, images, homography, invhomography):

        # TODO: get greatest height and width of image sets

        image = images[1]
        I_WIDTH = image.shape[1]
        I_HEIGHT = image.shape[0]
        corners_array = []
        corners_array.append(self.project(I_WIDTH, 0, invhomography))
        corners_array.append(self.project(I_WIDTH, I_HEIGHT, invhomography))
        corners_array.append(self.project(0, 0, invhomography))
        corners_array.append(self.project(0, I_HEIGHT, invhomography))
        # tr_x, tr_y = self.project(I_WIDTH, 0, invhomography)
        # br_x, br_y = self.project(I_WIDTH, I_HEIGHT, invhomography)
        # tl_x, tl_y = self.project(0, 0, invhomography)
        # bl_x, bl_y = self.project(0, I_HEIGHT, invhomography)

        # TODO: create a blank image with that information
        new_img_w = I_WIDTH
        new_img_h = I_HEIGHT
        toaddw = 0
        toaddh = 0
        for corner in corners_array:

            if corner[0] < 0:
                toaddw = np.absolute(corner[0])
            elif corner[0] > new_img_w:
                new_img_w = corner[0]

            if corner[1] < 0:
                toaddh = np.absolute(corner[1])
            elif corner[1] > new_img_h:
                new_img_h = corner[1]

        new_img_w += toaddw
        new_img_h += toaddh
        # new_img_w = tr_x if tr_x > br_x else br_x
        # new_img_h = br_y if br_y > bl_y else bl_y

        blank_image = np.zeros((int(new_img_h), int(new_img_w), 3), np.uint8)
        imgshape = blank_image.shape

        # TODO: copy the first image into the blank image
        image = images[0]
        offset = blank_image.shape[0] - I_HEIGHT
        blank_image[offset-10:I_HEIGHT+offset-10, :I_WIDTH] = image[:I_HEIGHT, :I_WIDTH]



        # TODO: use project() on every pixel of the new image and check if it lies within image2
        patch_size = (2, 2)
        for y, row in enumerate(blank_image):
            for x, pixel in enumerate(row):
                x_2, y_2 = self.project(x, y, homography)
                # TODO: if the pixel lies within the second image then cpy its values onto the new image
                if 0 <= x_2 <= I_WIDTH and 0 <= y_2 <= I_HEIGHT:
                    # patch = cv2.getRectSubPix(images[1], (4, 4), (x_2, y_2))
                    # if blank_image[y - patch_size[0]: y + patch_size[0], x-patch_size[1]: x + patch_size[1], :].shape == (4, 4, 3):
                    #
                    #     shape = blank_image[x - patch_size[0]: x + patch_size[0], y-patch_size[1]: y + patch_size[1], :]
                    #     # patch = patch.reshape(shape)
                    #
                    #     blank_image[y-patch_size[0]:y+patch_size[0], x-patch_size[1]: x+patch_size[1], :] = patch

                    blank_image[y+offset-10, x] = images[1][int(y_2), int(x_2)]
            # x = i % (int(new_img_h)+1)
            # y = y + 1 if i % (int(new_img_h)+1) == 0 and i != 0 else y





        return blank_image
